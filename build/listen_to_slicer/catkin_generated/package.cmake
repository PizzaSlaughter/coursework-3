set(_CATKIN_CURRENT_PACKAGE "listen_to_slicer")
set(listen_to_slicer_VERSION "0.0.0")
set(listen_to_slicer_MAINTAINER "ambra <ambra@todo.todo>")
set(listen_to_slicer_PACKAGE_FORMAT "2")
set(listen_to_slicer_BUILD_DEPENDS "rospy" "sensor_msg" "std_msgs")
set(listen_to_slicer_BUILD_EXPORT_DEPENDS "rospy" "sensor_msg" "std_msgs")
set(listen_to_slicer_BUILDTOOL_DEPENDS "catkin")
set(listen_to_slicer_BUILDTOOL_EXPORT_DEPENDS )
set(listen_to_slicer_EXEC_DEPENDS "rospy" "sensor_msg" "std_msgs")
set(listen_to_slicer_RUN_DEPENDS "rospy" "sensor_msg" "std_msgs")
set(listen_to_slicer_TEST_DEPENDS )
set(listen_to_slicer_DOC_DEPENDS )
set(listen_to_slicer_URL_WEBSITE "")
set(listen_to_slicer_URL_BUGTRACKER "")
set(listen_to_slicer_URL_REPOSITORY "")
set(listen_to_slicer_DEPRECATED "")