# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/ambra/catkin_ros/src/ROS-IGTL-Bridge/src/rib_converter_base.cpp" "/home/ambra/catkin_ros/build/ROS-IGTL-Bridge/CMakeFiles/ros_igtl_bridge_lib.dir/src/rib_converter_base.cpp.o"
  "/home/ambra/catkin_ros/src/ROS-IGTL-Bridge/src/rib_converter_image.cpp" "/home/ambra/catkin_ros/build/ROS-IGTL-Bridge/CMakeFiles/ros_igtl_bridge_lib.dir/src/rib_converter_image.cpp.o"
  "/home/ambra/catkin_ros/src/ROS-IGTL-Bridge/src/rib_converter_manager.cpp" "/home/ambra/catkin_ros/build/ROS-IGTL-Bridge/CMakeFiles/ros_igtl_bridge_lib.dir/src/rib_converter_manager.cpp.o"
  "/home/ambra/catkin_ros/src/ROS-IGTL-Bridge/src/rib_converter_point.cpp" "/home/ambra/catkin_ros/build/ROS-IGTL-Bridge/CMakeFiles/ros_igtl_bridge_lib.dir/src/rib_converter_point.cpp.o"
  "/home/ambra/catkin_ros/src/ROS-IGTL-Bridge/src/rib_converter_pointcloud.cpp" "/home/ambra/catkin_ros/build/ROS-IGTL-Bridge/CMakeFiles/ros_igtl_bridge_lib.dir/src/rib_converter_pointcloud.cpp.o"
  "/home/ambra/catkin_ros/src/ROS-IGTL-Bridge/src/rib_converter_polydata.cpp" "/home/ambra/catkin_ros/build/ROS-IGTL-Bridge/CMakeFiles/ros_igtl_bridge_lib.dir/src/rib_converter_polydata.cpp.o"
  "/home/ambra/catkin_ros/src/ROS-IGTL-Bridge/src/rib_converter_string.cpp" "/home/ambra/catkin_ros/build/ROS-IGTL-Bridge/CMakeFiles/ros_igtl_bridge_lib.dir/src/rib_converter_string.cpp.o"
  "/home/ambra/catkin_ros/src/ROS-IGTL-Bridge/src/rib_converter_transform.cpp" "/home/ambra/catkin_ros/build/ROS-IGTL-Bridge/CMakeFiles/ros_igtl_bridge_lib.dir/src/rib_converter_transform.cpp.o"
  "/home/ambra/catkin_ros/src/ROS-IGTL-Bridge/src/ros_igtl_bridge.cpp" "/home/ambra/catkin_ros/build/ROS-IGTL-Bridge/CMakeFiles/ros_igtl_bridge_lib.dir/src/ros_igtl_bridge.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "QT_WIDGETS_LIB"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"ros_igtl_bridge\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/ambra/catkin_ros/devel/include"
  "/home/ambra/catkin_ros/src/ROS-IGTL-Bridge/include"
  "/home/ambra/catkin_ros/src/ROS-IGTL-Bridge/testing/include"
  "/usr/include/vtk-6.3"
  "/usr/include/freetype2"
  "/usr/lib/x86_64-linux-gnu/openmpi/include/openmpi"
  "/usr/lib/x86_64-linux-gnu/openmpi/include/openmpi/opal/mca/event/libevent2022/libevent"
  "/usr/lib/x86_64-linux-gnu/openmpi/include/openmpi/opal/mca/event/libevent2022/libevent/include"
  "/usr/lib/x86_64-linux-gnu/openmpi/include"
  "/usr/include/python2.7"
  "/usr/include/x86_64-linux-gnu"
  "/usr/include/hdf5/openmpi"
  "/usr/include/libxml2"
  "/usr/include/jsoncpp"
  "/usr/include/tcl"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/opencv"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++"
  "/home/ambra/OpenIGTLink-build"
  "/home/ambra/OpenIGTLink/Source"
  "/home/ambra/OpenIGTLink-build/Source"
  "/home/ambra/OpenIGTLink/Source/igtlutil"
  "/home/ambra/OpenIGTLink-build/Source/igtlutil"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
