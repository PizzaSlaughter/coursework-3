# CMake generated Testfile for 
# Source directory: /home/ambra/catkin_ros/src
# Build directory: /home/ambra/catkin_ros/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("listen_to_slicer")
subdirs("ROS-IGTL-Bridge")
