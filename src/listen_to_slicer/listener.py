#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import geometry_msgs.msg
from ros_igtl_bridge.msg import igtltransform
import moveit_commander
import moveit_msgs.msg
from moveit_commander.conversions import pose_to_list


def callback(data):
    rospy.loginfo("%0.2f is my xCoordinate, %0.2f is my yCoordinate, %0.2f is my zCoordinate", data.transform.translation.x,data.transform.translation.y, data.transform.translation.z)
def go_to_pose_goal(self):
    # Copy class variables to local variables to make the web tutorials more clear.
    # In practice, you should use the class variables directly unless you have a good
    # reason not to.
    move_group = self.move_group

    ## BEGIN_SUB_TUTORIAL plan_to_pose
    ##
    ## Planning to a Pose Goal
    ## ^^^^^^^^^^^^^^^^^^^^^^^
    ## We can plan a motion for this group to a desired pose for the
    ## end-effector:
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = 1.0
    pose_goal.position.x = data.transform.translation.x
    pose_goal.position.y = data.transform.translation.y
    pose_goal.position.z = data.transform.translation.z

    move_group.set_pose_target(pose_goal)

    ## Now, we call the planner to compute the plan and execute it.
    plan = move_group.go(wait=True)
    # Calling `stop()` ensures that there is no residual movement
    move_group.stop()
    # It is always good to clear your targets after planning with poses.
    # Note: there is no equivalent function for clear_joint_value_targets()
    move_group.clear_pose_targets()

    ## END_SUB_TUTORIAL

    # For testing:
    # Note that since this section of code will not be included in the tutorials
    # we use the class variable rather than the copied state variable
    current_pose = self.move_group.get_current_pose().pose
    return all_close(pose_goal, current_pose, 0.01)
    
    
def listener():


    rospy.init_node('listener', anonymous=True, log_level=rospy.INFO)

    rospy.Subscriber('/IGTL_TRANSFORM_IN', igtltransform, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
